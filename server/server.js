
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var app = express();

mongoose.connect("mongodb://localhost/nodejs_crud");

// Model Declaration

var employees = mongoose.model('employees', {
    firstname: { type: String },
    lastname: { type: String },
    email: { type: String },
    location: { type: String },
    salary: { type: Number }
});


app.use(bodyParser.json());
app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});


app.post('/addEmployee',function(req,res){
    console.log('EMPLOYEE POST',req.body);
    var employee = new employees(req.body);
    employee.save(function(err,result){
        if(err){ 
            res.send({'message':'error'});
        } else {
            res.send({'message':'success'});
        }
    });
});

app.get('/getEmployee',function(req,res){
    employees.find(function(err,result){
        if(err){
            res.send({'message':'error'})
        } else {
            res.send(result);
        }
    })
})

app.get('/empDetails/:id',function(req,res){    
    var query = {
        "_id": req.params.id
    }
    employees.findOne(query,function(err,result){
        if(err){
            res.send({'message':'error'})
        } else {
            res.send(result);
        }
    })
})

app.put('/updateEmp/:id',function(req,res){
    var query = {
        "_id": req.params.id
    }
    var update = req.body;

    employees.findOneAndUpdate(query,update).exec(function(err){
        if(err){
            res.send({'message':'error'})
        } else {
            res.send({'message':'success'})
        }
    })
})

app.delete('/deleteEmp/:id',function(req,res){
    var query = {
        "_id":req.params.id 
    }
    employees.findOne(query).exec(function(err,data){
        if(err){            
            res.send({'message':'error'})
        } else {            
            data.remove();            
            res.send({'message':'success'})
        }
    })
})

app.listen(8080);
console.log("Server Running at port 8080")

