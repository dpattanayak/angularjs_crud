angular.module('crud',[]).controller('crudCtrl', ['$scope','$http', function($scope,$http) {
  
    $scope.createEmp = function(){
        var employee = {
            headers: {
                "Content-Type":"application/json"
            },
            url: $scope.api + "addEmployee",
            data: $scope.emp,
            method: "POST"
        }
        $http(employee).then(function(res){
            if(res){
                $scope.add = false;
                $scope.getEmp();
            }
        });

    }

    $scope.getEmp = function(){
        var employee = {
            headers: {
                "Content-Type":"application/json"
            },
            url: $scope.api + "getEmployee",
            method: "GET"
        }
        $http(employee).then(function(res){
            $scope.employeeData = res.data;            
        })
    }

    $scope.viewEmp = function(id){
        var employee = {
            headers: {
                "Content-Type":"application/json"
            },
            url: $scope.api + "empDetails/" + id,            
            method: "GET"
        }
        $http(employee).then(function(res){
            $scope.emp = res.data;             
            $(".employeeData").hide();
            $scope.view = true;         
        })
    }

    $scope.editEmp = function(id){
        var employee = {
            headers: {
                "Content-Type":"application/json"
            },
            url: $scope.api + "empDetails/" + id,            
            method: "GET"
        }
        $http(employee).then(function(res){
            $scope.emp = res.data;             
            $scope.add = true;
            $scope.update = true;
            $(".employeeData").hide();        
        })                
    }

    $scope.updateEmp = function(){
        var employee = {
            headers: {
                "Content-Type":"application/json"
            },
            url: $scope.api + "updateEmp/" + $scope.emp._id, 
            data: $scope.emp,           
            method: "PUT"
        }
        $http(employee).then(function(res){
            $scope.getEmp();
            $scope.add = false;
            $(".employeeData").show();    
        })
    }

    $scope.deleteEmp = function(id){
        var employee = {
            headers: {
                "Content-Type":"application/json"
            },
            url: $scope.api + "deleteEmp/" + id, 
            data: $scope.emp,           
            method: "DELETE"
        }
        $http(employee).then(function(res){
            $scope.getEmp();              
        })
    }

    $scope.back = function(){        
        $scope.view = false;
        $scope.add = false;
        $(".employeeData").show();
        $scope.emp = {};
    }


    $scope.init = function(){
        $scope.api = "http://localhost:8080/";
        $scope.add = false;
        $scope.view = false;
        $scope.update = false;
        $scope.emp = {};
        $scope.getEmp();
    }
    
    $scope.init();
}]);